use std::process;

use rand::{rngs::ThreadRng, Rng};
use regex::Regex;

pub fn parse_format(args: &[String]) -> Option<String> {
    let mac_regexp = Regex::new(r"^([0-9a-fx]{2})(:[0-9a-fx]{2}){5}$").unwrap();
    let mac_arg = args.get(1)?;
    let mac_arg_lower = mac_arg.to_lowercase();

    match mac_regexp.is_match(&mac_arg_lower) {
        true => Some(mac_arg_lower),
        false => None,
    }
}

pub fn print_usage() -> ! {
    println!("MAC Address generator");
    println!();
    println!("Description:");
    println!();
    println!("\t Generates MAC addresses of a given format.");
    println!();
    println!("Usage:");
    println!();
    println!("\t$ generate-mac-address <format>");
    println!();
    println!(
        "\tWhere <format> is a string of format \"XX:XX:XX:XX:XX:XX\" where each \"X\" can be:"
    );
    println!("\t- either a case-insensitive hexadecimal digit;");
    println!("\t- or a case-insensitive placeholder \"X\" letter, which will be replaced.");
    println!();
    println!("Author: Mihai Bojescu");
    println!();

    #[cfg(not(test))]
    process::exit(1);
    #[cfg(test)]
    process::exit(0);
}

pub fn generate(random_generator: &mut ThreadRng, mac_format: &str) -> Option<[u8; 6]> {
    let mut mac: [u8; 6] = [0x00, 0x00, 0x00, 0x00, 0x00, 0x00];
    let mut current_mac_byte = 0;
    let mac_slices = mac_format.split(':').collect::<Vec<&str>>();

    if mac_slices.len() > mac.len() {
        return None;
    }

    for mac_slice in mac_slices {
        for (multiplier, mac_slice_char) in mac_slice.chars().enumerate() {
            let mac_slice_u8 = mac_slice_char as u8;

            let value = match mac_slice_u8 {
                48..=57 => mac_slice_u8 - 48,
                97..=102 => mac_slice_u8 - 87,
                _ => random_generator.gen_range(0..=15),
            };
            let digit = match multiplier {
                0 => 16,
                _ => 1,
            };

            mac[current_mac_byte] += value * digit;
        }

        current_mac_byte += 1;
    }

    (current_mac_byte..mac.len()).for_each(|i| {
        mac[i] = random_generator.gen_range(0..=255);
    });

    Some(mac)
}

pub fn print(mac: [u8; 6]) {
    println!(
        "{:02X}:{:02X}:{:02X}:{:02X}:{:02X}:{:02X}",
        mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]
    );
}

#[cfg(test)]
mod tests {
    use super::*;

    mod parse_mac_format {
        use super::*;

        #[test]
        fn test_arg_get() {
            let result = parse_format(&[]);

            assert_eq!(result, None);
        }

        #[test]
        fn test_arg_no_regex_match() {
            let result = parse_format(&[String::from(""), String::from("x:x:x:x:x:x")]);

            assert_eq!(result, None);
        }

        #[test]
        fn test_arg_with_placeholders_values_regex_match() {
            let result = parse_format(&[String::from(""), String::from("xx:xx:xx:xx:xx:xx")]);

            assert_eq!(result, Some(String::from("xx:xx:xx:xx:xx:xx")));
        }

        #[test]
        fn test_arg_with_concrete_values_regex_match() {
            let result = parse_format(&[String::from(""), String::from("ff:ff:ff:ff:ff:ff")]);

            assert_eq!(result, Some(String::from("ff:ff:ff:ff:ff:ff")));
        }

        #[test]
        fn test_arg_with_placeholder_and_concrete_values_regex_match() {
            let result = parse_format(&[String::from(""), String::from("fx:fx:fx:fx:fx:fx")]);

            assert_eq!(result, Some(String::from("fx:fx:fx:fx:fx:fx")));
        }

        #[test]
        fn test_arg_case_insensitive_regex_match() {
            let result = parse_format(&[String::from(""), String::from("Fx:fX:FX:aB:Cd:eF")]);

            assert_eq!(result, Some(String::from("fx:fx:fx:ab:cd:ef")));
        }
    }

    mod print_usage {
        use super::*;

        #[test]
        #[should_panic]
        fn test_exit() {
            print_usage();
        }
    }

    mod generate_mac_address {
        use super::*;

        #[test]
        fn test_wrong_format_length() {
            let result = generate(
                &mut rand::thread_rng(),
                &String::from("xx:xx:xx:xx:xx:xx:xx"),
            );

            assert_eq!(result, None);
        }

        #[test]
        fn test_generate_with_concrete_values_format() {
            let result =
                generate(&mut rand::thread_rng(), &String::from("00:00:00:00:00:00"));

            assert_eq!(result, Some([0x00, 0x00, 0x00, 0x00, 0x00, 0x00]));
        }

        #[test]
        fn test_generate_with_placeholder_values_format() {
            let result =
                generate(&mut rand::thread_rng(), &String::from("xx:xx:xx:xx:xx:xx"));

            assert_ne!(result, None);
        }

        #[test]
        fn test_generate_with_concrete_and_placeholder_values_format() {
            let result =
                generate(&mut rand::thread_rng(), &String::from("xx:ff:xx:ff:xx:ff"));

            assert_ne!(result, None);
            assert_eq!(result.unwrap()[1], 0xFF);
            assert_eq!(result.unwrap()[3], 0xFF);
            assert_eq!(result.unwrap()[5], 0xFF);
        }
    }

    mod print_mac_address {
        use super::*;

        #[test]
        fn test_print() {
            print([0x00, 0x00, 0x00, 0x00, 0x00, 0x00]);
        }
    }
}
