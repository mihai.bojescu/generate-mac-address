mod mac;

use std::env;

fn main() {
    let args = env::args().collect::<Vec<String>>();
    let mut generator = rand::thread_rng();

    let mac_format = mac::parse_format(&args);
    let mac_address = match mac_format {
        Some(value) => mac::generate(&mut generator, &value),
        None => mac::print_usage(),
    };

    match mac_address {
        Some(value) => mac::print(value),
        None => mac::print_usage(),
    }
}
