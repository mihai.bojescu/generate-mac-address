# generate-mac-address

## Description

A small project that can be used to generate MAC addresses using a given format.

## Building

Run the following command:

```bash
$ cargo build
```

Or if building for `release`, run the following command:

```bash
$ cargo build --release
```
